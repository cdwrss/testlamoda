from django.db import models
from django.utils import  timezone

# Create your models here.

class Order(models.Model):
    client = models.CharField(max_length=200)
    index = models.CharField(max_length=150)
    mail_id = models.CharField(max_length=200)
    weight = models.FloatField(max_length=200)
    price_amount = models.IntegerField()

    def __str__(self):
        return self.index

class File(models.Model):
    file = models.FileField(blank=False, null=False)

