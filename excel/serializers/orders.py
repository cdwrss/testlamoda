from rest_framework import serializers
from excel.models import File



class OrderSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    client = serializers.CharField()
    index = serializers.CharField()
    mail_id = serializers.CharField()
    weight = serializers.FloatField()
    price_amount = serializers.IntegerField()

class FileSerializer(serializers.ModelSerializer):
  class Meta():
    model = File
    fields = ('file',)