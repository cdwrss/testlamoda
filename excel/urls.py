from .views import OrderViewSet, FileView, api_root
from django.urls import path

snippet_list = OrderViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

urlpatterns = [
    path('', api_root),
    path('orders/', snippet_list, name='order-list'),
    path('upload/', FileView.as_view(), name='file-upload'),
]

