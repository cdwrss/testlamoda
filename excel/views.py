
from .models import Order
from django.http import Http404
from .serializers.orders import OrderSerializer, FileSerializer
from openpyxl import load_workbook
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework import status
from django.conf import settings
from rest_framework import viewsets
from openpyxl import load_workbook
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse


# Create your views here

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'orders': reverse('order-list', request=request, format=format)
    })

class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.order_by('-id')
    serializer_class = OrderSerializer


def upload_data(path):

    wb = load_workbook(filename=settings.BASE_DIR+path, read_only=True)
    for ws in wb:
        for row in ws.rows:
            try:
                order = Order(
                    client=row[0].value,
                    index=row[1].value,
                    mail_id=row[2].value,
                    weight=row[3].value,
                    price_amount=row[4].value)
                order.save()
            except Exception as e:
                pass

class FileView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            upload_data(file_serializer.data.get('file'))
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)